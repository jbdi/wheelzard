# coding: utf-8
from django.contrib import admin

from django_mptt_admin.admin import DjangoMpttAdmin

from cpa.vseinstrumenti.models import VseinstrumentiCategory, Vseinstrumenti


@admin.register(VseinstrumentiCategory)
class ProballCategoryAdmin(DjangoMpttAdmin):
    pass


@admin.register(Vseinstrumenti)
class ProballAdmin(admin.ModelAdmin):
    list_display = ('name', 'price')
