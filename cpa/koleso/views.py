# coding: utf-8
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView, TemplateView

from pure_pagination.mixins import PaginationMixin

from cpa.koleso.models import (Tire, Disk, CatalogCategory,
                               TireVendor, DiskVendor)
from cpa.koleso.filters import DiskFilter, TireFilter
from cpa.geo.models import City


DISK_CATEGORY_ID = 2
TIRE_CATEGORY_ID = 3


class KolesoIndexView(TemplateView):
    template_name = 'koleso/index.jinja'

    def get_context_data(self, **kwargs):
        context = super(KolesoIndexView, self).get_context_data(**kwargs)

        context['disks_diameters'] = Disk.objects.order_by('diameter').filter(diameter__gte=12) \
            .values_list('diameter', flat=True).distinct()
        context['tires_diameters'] = Tire.objects.order_by('diameter') \
            .values_list('diameter', flat=True).distinct()

        # NOTE: вместо фильтрации по filter(items__enabled=True).order_by('name').distinct()
        #       можно писать, что сейчас товары из этой категории недоступны и показывать недоступные товары с неактивным цветом
        context['disk_brands'] = DiskVendor.objects.all()
        context['tire_brands'] = TireVendor.objects.all()
        return context


class KolesoCitiesView(TemplateView):
    template_name = 'koleso/cities.jinja'

    def get_context_data(self, **kwargs):
        context = super(KolesoCitiesView, self).get_context_data(**kwargs)
        context['cities'] = City.objects.filter(site__isnull=False) \
                                        .prefetch_related('site')
        return context


"""
добавить проверку принадлежности id товара к бренду
"""


class DiskDetailView(DetailView):
    model = Disk
    template_name = 'koleso/detail-disk.jinja'

    def get_related(self):
        item = self.get_object()
        return self.get_queryset().filter(width=item.width, diameter=item.diameter, edge=item.edge).exclude(id=item.id).order_by('?')[:4]

    def get_context_data(self, **kwargs):
        context = super(DiskDetailView, self).get_context_data(**kwargs)
        context['category_uri'] = 'disks'
        context['related_items'] = self.get_related()
        return context

    def get_queryset(self):
        vendor = get_object_or_404(DiskVendor, name=self.kwargs.get('brand'))
        return self.model.objects.active_items().filter(vendor=vendor)


class TireDetailView(DetailView):
    model = Tire
    template_name = 'koleso/detail-tire.jinja'

    def get_related(self):
        item = self.get_object()
        return self.get_queryset().filter(width=item.width, height=item.height, diameter=item.diameter).exclude(id=item.id).order_by('?')[:4]

    def get_context_data(self, **kwargs):
        context = super(TireDetailView, self).get_context_data(**kwargs)
        context['category_uri'] = 'tires'
        context['related_items'] = self.get_related()
        return context


class BaseListView(PaginationMixin, ListView):
    template_name = 'koleso/list.jinja'
    context_object_name = 'items'
    paginate_by = 12
    paginate_orphans = 0

    def get_queryset(self):
        qs = self.model.objects.active_items()
        if self.brand:
            qs = qs.filter(vendor=self.brand)

        if self.kwargs.get('diameter'):
            qs = qs.filter(diameter=self.kwargs.get('diameter'))
        return qs.prefetch_related('vendor')

    def get(self, request, *args, **kwargs):
        brand = self.kwargs.get('brand')
        self.brand = None
        if brand:
            self.brand = get_object_or_404(self.vendor_model, name=brand)
        return super(BaseListView, self).get(request, *args, **kwargs)


class DiskItemListView(BaseListView):
    model = Disk
    object = Disk
    vendor_model = DiskVendor

    def get_context_data(self, **kwargs):
        context = super(DiskItemListView, self).get_context_data(**kwargs)
        context['filter'] = DiskFilter(self.request.GET, self.get_queryset())
        context['category'] = get_object_or_404(
            CatalogCategory, id=DISK_CATEGORY_ID)
        context['category_uri'] = 'disks'
        context['brand'] = self.brand
        context['diameter'] = self.kwargs.get('diameter')
        return context

    def get_queryset(self):
        qs = super(DiskItemListView, self).get_queryset()
        return DiskFilter(self.request.GET, qs).qs


class TireItemListView(BaseListView):
    model = Tire
    object = Tire
    vendor_model = TireVendor

    def get_context_data(self, **kwargs):
        context = super(TireItemListView, self).get_context_data(**kwargs)
        context['filter'] = TireFilter(self.request.GET, self.get_queryset())
        context['category'] = get_object_or_404(
            CatalogCategory, id=TIRE_CATEGORY_ID)
        context['category_uri'] = 'tires'
        context['brand'] = self.brand
        context['diameter'] = self.kwargs.get('diameter')
        return context

    def get_queryset(self):
        qs = super(TireItemListView, self).get_queryset()
        return TireFilter(self.request.GET, qs).qs
