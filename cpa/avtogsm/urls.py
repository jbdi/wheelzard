# from django.conf import settings
from django.conf.urls import patterns, include, url
from django.views.decorators.cache import cache_page

from cpa.avtogsm.views import ItemsListView, ItemDetailView

cache_view = lambda v: cache_page(60 * 60)(v)

urlpatterns = patterns('',
    url(r'^$', include('pure_pagination.urls')),

    url(r'^$', cache_view(ItemsListView.as_view()), name='list'),
    url(r'^category(?P<category_id>\d+)/?$', cache_view(ItemsListView.as_view()), name='list'),
    url(r'^category(?P<category_id>\d+)/item(?P<pk>\d+)/$', cache_view(ItemDetailView.as_view()), name='detail'),
)
