# coding: utf-8
import csv
from datetime import datetime

from django.core.management.base import BaseCommand
from django.conf import settings

import pytz

from cpa.koleso.models import CatalogVendor as Vendor, CatalogCategory as Category, Sportiv


# """
# Скачивание файла
# Импорт файла
#     - подсчет кол-ва товаров в файле
# """

ROOT_CATEGORY_ID = 2

class Command(BaseCommand):
    # сделать отдельной процедурой подсчет(перерасчет) кол-ва товаров в категории
    def handle(self, *args, **options):
        csv_reader('sportiv_products_20160301_164416.csv', run_import)
        # print "amount: %d, created: %d, updated: %d" % (cnt, created_cnt, updated_cnt)


def run_import(row, model=Sportiv):
    ALLOWED_FIELDS = (
        'available',
        'description',
        'item_id',
        'item_type',
        'modified_time',
        'name',
        'price',
        'oldprice',
        'param',
        'picture',
        'url',
        'vendor',
        'category_id',

        'country_of_origin',

        # 'delivery',
        # 'adult',
        # 'local_delivery_cost',
        # 'model',
        # 'pickup',
    )

    row_columns = row.keys()
    for field_name in row_columns:
        if not field_name in ALLOWED_FIELDS:
            del row[field_name]

    vendor, _ = Vendor.objects.get_or_create(name=row['vendor'].strip().capitalize())
    row['vendor'] = vendor
    row['oldprice'] = float(row['oldprice'] or 0)
    row['price'] = float(row['price'] or 0)
    row['modified_time'] = datetime.utcfromtimestamp(int(row['modified_time'])).replace(tzinfo=pytz.utc)

    row['category_id'] = make_categories_tree(row['category_id'])
    row['item_id'] = row['item_id']
    row['enabled'] = True

    row = {col: unicode(val, 'utf-8') if isinstance(val, str) else val for col, val in row.items()}
    item, created = model.objects.update_or_create(item_id=row['item_id'], defaults=row)
    counters = {}
    counters['handled'] = 1
    if created:
        counters['created'] = 1
        counters['updated'] = 0
    else:
        counters['created'] = 0
        counters['updated'] = 1
    return counters


def make_categories_tree(categories_string=''):
    """
    делает из строки "Category/subcategory/sub subcategory" дерево категорий в бд
    """
    parent = Category.objects.get(id=ROOT_CATEGORY_ID)
    categories = enumerate(categories_string.split('/'), start=0)
    for index, category_name in categories:
        category, created = Category.objects.get_or_create(name=category_name, parent=parent)
        parent = category
    return category.id


def csv_reader(file_name, row_handler):
    file_path = settings.BASE_DIR + '/' + file_name

    with open(file_path, 'r') as line:
        reader = csv.DictReader(line, delimiter=';')

        # # также можно припихнуть проверку на слишком маленкое кол-во товаров
        # cursor.execute('truncate catalog_item')
        handled = 0
        created = 0
        updated = 0
        for row in reader:
            counters = row_handler(row)
            handled += counters['handled']
            created += counters['created']
            updated += counters['updated']
        print "handled: %d, created: %d, updated: %d" % (handled, created, updated)
