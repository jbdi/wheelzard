from .common import *


DEBUG = False
ALLOWED_HOSTS = ['wheelzard.ru', '.wheelzard.ru']


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'wheelzard.ru',
        'USER': 'wheelzard.ru',
        'PASSWORD': '',
        'HOST': '127.0.0.1'
    }
}

cache_backend = 'django.core.cache.backends.filebased.FileBasedCache'
CACHES = {
    'default': {
        'BACKEND': cache_backend,
        'LOCATION': os.path.join(BASE_DIR, 'cache'),
        'TIMEOUT': 60 * 60,
        'OPTIONS': {
            'MAX_ENTRIES': 20000
        }
    }
}


TEMPLATES = [
    {
        'BACKEND': 'django_jinja.backend.Jinja2',
        'DIRS': [os.path.join(os.path.join(BASE_DIR, 'cpa'), 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            # Match the template names ending in .html but not the ones in the admin folder.
            "match_extension": ".jinja",
            "match_regex": r"^(?!admin/).*",
            "app_dirname": "templates",
            "bytecode_cache": {
                "name": "default",
                "backend": "django_jinja.cache.BytecodeCache",
                "enabled": False,
            },
            "autoescape": True,
            "auto_reload": DEBUG,
            # "translation_engine": "django.utils.translation",

            # Can be set to "jinja2.Undefined" or any other subclass.
            "undefined": None,
            "globals": {
                'bootstrap_css': 'bootstrap3.templatetags.bootstrap3.bootstrap_css',
                'bootstrap_javascript': 'bootstrap3.templatetags.bootstrap3.bootstrap_javascript',
            },
            # "extensions": DEFAULT_EXTENSIONS + [
            #     "endless_pagination.templatetags.endless.paginate"
            # ],
            'context_processors': [
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.static',
                "django.template.context_processors.request",
                "cpa.context_processors.cities",
            ],
        },
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'cpa', 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.static',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                "django.template.context_processors.request",
            ],
        },
    }
]
