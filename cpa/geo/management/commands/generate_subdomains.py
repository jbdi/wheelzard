# coding: utf-8
from __future__ import unicode_literals

import re

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.sites.models import Site

import pymorphy2

from cpa.geo.models import City


DOMAIN = 'localhost:8000' if settings.DEBUG else 'wheelzard.ru'
MAIN_DOMAIN = 'moscow'

morph = pymorphy2.MorphAnalyzer()


class Command(BaseCommand):

    def handle(self, *args, **options):
        cities = City.objects.filter(population__gte=500000)
        for city in cities:
            full_subdomain = get_full_subdomain(city.name)
            if get_subdomain(city.name) == MAIN_DOMAIN:
                continue

            site_instance, _ = Site.objects.update_or_create(domain=full_subdomain, name=city.name)
            city.site = site_instance
            city.ru_names = get_ru_cities(city.alternate_names)
            city.case_where = get_city_where(city.ru_names)
            city.save()

        site_instance, _ = Site.objects.update_or_create(domain=DOMAIN, name=MAIN_DOMAIN)
        moskow = City.objects.get(name__icontains=MAIN_DOMAIN)
        moskow.site = site_instance
        moskow.ru_names = get_ru_cities(moskow.alternate_names)
        moskow.case_where = get_city_where(moskow.ru_names)
        moskow.save()

def get_subdomain(eng_city_name):
    return ''.join([i if ord(i) < 128 else '' for i in eng_city_name.lower()])

def get_full_subdomain(eng_city_name):
    subdomain = get_subdomain(eng_city_name)
    full_subdomain = '%s.%s' % (subdomain, DOMAIN)
    full_subdomain = re.sub(r'[^\x00-\x7F]+|[\s\'\"\-]', '', full_subdomain)
    return full_subdomain

def get_ru_cities(cities_text):
    preg_ru_cities = re.search(ur'([а-я-]+?,?[а-я\s]+)', cities_text, flags=re.U|re.I)
    return preg_ru_cities.group()

def get_city_where(city_ru):
    exeptions = {u'Набережные Челны': 'набережных челнах'}
    exept = exeptions.get(city_ru)
    if exept:
        return exept

    grammems = morph.parse(city_ru)
    return grammems[0].inflect({'sing', 'loct'}).word

