# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avtogsm', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='avtogsm',
            name='seo_name',
            field=models.CharField(max_length=512, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='avtogsmcatalogcategory',
            name='slug',
            field=models.SlugField(default=b'-', max_length=100),
        ),
    ]
