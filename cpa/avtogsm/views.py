# coding: utf-8
from operator import attrgetter

from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView, TemplateView
from pure_pagination.mixins import PaginationMixin

from cpa.avtogsm.models import AvtogsmCatalogCategory, Avtogsm


KOVRIKI_CATEGORY_ID = 41


def get_menu_context(self, context, **kwargs):
    category_id = int(self.kwargs.get('category_id', 0))

    context['category_id'] = category_id
    context['menu_subcategories'] = []
    current_category = None

    parents_ids = []
    if category_id:
        current_category = AvtogsmCatalogCategory.objects.get(id=category_id)
        parents_ids = current_category.get_ancestors(ascending=False, include_self=False).values_list('id', flat=True)
        context['menu_subcategories'] = current_category.get_children().select_related().filter(level__gte=2)
    context['parents_ids'] = parents_ids
    context['current_category'] = current_category

    more_than_one_subcategory = len(context['menu_subcategories']) < 2
    without_subcategories = not context['menu_subcategories']
    is_not_main_menu_category = current_category.level >= 2 if current_category else None
    if (without_subcategories or more_than_one_subcategory) and current_category and is_not_main_menu_category:
        parent_subcategories = current_category.parent.get_children().select_related().filter(level__gte=1)
        context['menu_subcategories'] = parent_subcategories

    context['menu_categories'] = AvtogsmCatalogCategory.objects.get(name='main_root').get_children().exclude(name='ROOT').select_related()
    context['category_title'] = current_category.name if current_category else u'Каталог автоаксессуаров'

    ancestors = current_category.get_ancestors(ascending=False, include_self=True)[1:] if current_category else []
    context['reverted_path_way'] = reversed(map(attrgetter('name'), ancestors))
    return context


class ItemDetailView(DetailView):
    model = Avtogsm
    template_name = 'avtogsm/detail.jinja'

    def get_context_data(self, **kwargs):
        context = super(ItemDetailView, self).get_context_data(**kwargs)
        menu_context = get_menu_context(self, context, **kwargs)
        context.update(menu_context)
        return context

    def get_queryset(self):
        category = get_object_or_404(AvtogsmCatalogCategory, id=self.kwargs.get('category_id'))
        return self.model.objects.filter(category=category)


class ItemsListView(PaginationMixin, ListView):
    template_name = 'avtogsm/list.jinja'
    context_object_name = 'items'
    paginate_by = 12
    paginate_orphans = 0
    model = Avtogsm

    def get_queryset(self):
        category_id = int(self.kwargs.get('category_id', 0))
        if category_id:
            categories = AvtogsmCatalogCategory.objects.get(pk=category_id).get_descendants(include_self=True).values_list('id')
            qs = self.model.objects.filter(category__in=categories).prefetch_related('category').order_by('category')
        else:
            qs = self.model.objects.prefetch_related('category').order_by('category').all()
        return qs

    def get(self, request, *args, **kwargs):
        return super(ItemsListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ItemsListView, self).get_context_data(**kwargs)
        menu_context = get_menu_context(self, context, **kwargs)
        context.update(menu_context)
        return context


        context['menu_categories'] = AvtogsmCatalogCategory.objects.get(name='main_root').get_children().exclude(name='ROOT').select_related()
        return context

