#  coding: utf8

from django.db import models

from mptt.models import MPTTModel, TreeForeignKey

from cpa.koleso.models.models import *


class CatalogVendor(models.Model):
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'catalog_vendor'


class AbsCatalogCategory(MPTTModel):
    name = models.CharField(max_length=100, blank=True, default='', null=False)
    sort_pos = models.IntegerField(default=0, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='category')
    popular = models.BooleanField(default=False)
    seo_name = models.CharField(max_length=100, blank=True, default='', null=False)
    seo_title = models.CharField(max_length=250, blank=True, default='', null=False)
    seo_description = models.CharField(max_length=250, blank=True, default='', null=False)
    seo_keywords = models.CharField(max_length=250, blank=True, default='', null=False)
    text = models.TextField(blank=True, default='', null=False)

    def __unicode__(self):
        return self.name


    def get_absolute_url(self, kind):
        return reverse('koleso:list', args=[kind])

    class Meta:
        abstract = True

    class MPTTMeta:
        order_insertion_by = ['name']


class CatalogCategory(AbsCatalogCategory):
    class Meta:
        db_table = 'catalog_category'

