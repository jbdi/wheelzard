# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('geo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='site',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='sites.Site', null=True),
        ),
    ]
