# coding: utf8
from django_jinja import library
# import jinja2

@library.filter
def plural(n, *word_forms):
    """
    http://docs.translatehouse.org/projects/localization-guide/en/latest/l10n/pluralforms.html?id=l10n/pluralforms#r
    """
    n = int(n)
    if n%10==1 and n%100!=11:
        return word_forms[0]
    elif n%10>=2 and n%10<=4 and (n%100<10 or n%100>=20):
        return word_forms[1]
    else:
        return word_forms[2]