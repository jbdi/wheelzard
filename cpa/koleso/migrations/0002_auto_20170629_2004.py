# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('koleso', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='diskvendor',
            name='sorting_weight',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='tirevendor',
            name='sorting_weight',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
