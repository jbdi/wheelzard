# coding: utf8
from django.db import models
from django.core.urlresolvers import reverse

from mptt.models import MPTTModel, TreeForeignKey


class VseinstrumentiCategory(MPTTModel):
    name = models.CharField(max_length=100, blank=True, default='', null=False)
    sort_pos = models.IntegerField(default=0, null=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='category')
    popular = models.BooleanField(default=False)
    seo_name = models.CharField(max_length=100, blank=True, default='', null=False)
    seo_title = models.CharField(max_length=250, blank=True, default='', null=False)
    seo_description = models.CharField(max_length=250, blank=True, default='', null=False)
    seo_keywords = models.CharField(max_length=250, blank=True, default='', null=False)
    text = models.TextField(blank=True, default='', null=False)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self, kind):
        return reverse('koleso:list', args=[kind])

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        db_table = 'catalog_vseinstrumenticategory'


class Vseinstrumenti(models.Model):
    available = models.BooleanField(default=False)
    picture = models.URLField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    oldprice = models.FloatField(max_length=10, blank=True, null=True, default=0)
    url = models.URLField(max_length=512, blank=True, null=True)
    price = models.FloatField(max_length=10, blank=True, null=True, default=0)
    modified_time = models.DateTimeField(blank=True, null=True)
    item_id = models.CharField(max_length=128, blank=True, default='')
    # local_delivery_cost = models.FloatField(max_length=10, blank=True, null=True, default=0)
    item_type = models.CharField(max_length=512, blank=True, null=True)
    category = models.ForeignKey('VseinstrumentiCategory', related_name='items', null=True)
    name = models.CharField(max_length=512, blank=True, null=True)
    param = models.CharField(max_length=512, blank=True, null=True)
    enabled = models.BooleanField(default=False)

    class Meta:
        db_table = 'catalog_vseinstrumenti'
        ordering = ('price',)
