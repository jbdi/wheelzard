# from django.conf import settings
from django.conf.urls import patterns, include, url

from cpa.koleso.views import (DiskItemListView, TireItemListView,
    KolesoIndexView, KolesoCitiesView, DiskDetailView, TireDetailView)


urlpatterns = patterns('',
    url(r'^/?$', include('pure_pagination.urls')),

    url(r'cities/$', KolesoCitiesView.as_view(), name='cities'),
    url(r'^/?(?P<category_uri>disks)/$', DiskItemListView.as_view(), name='list'),
    url(r'^/?(?P<category_uri>disks)/d(?P<diameter>[\d\.]+)/$', DiskItemListView.as_view(), name='list'),
    url(r'^/?(?P<category_uri>disks)/brand/(?P<brand>[\w\d\-\&\s\%]+)/$', DiskItemListView.as_view(), name='list'),
    url(r'^/?(?P<category_uri>disks)/brand/(?P<brand>[\w\d\-\&\s\%]+)/d(?P<diameter>[\d\.]+)/$', DiskItemListView.as_view(), name='list'),
    url(r'^/?(?P<category_uri>disks)/brand/(?P<brand>[\w\d\-\&\s\%]+)/(?P<pk>\d+)/$', DiskDetailView.as_view(), name='detail'),

    url(r'^/?(?P<category_uri>tires)/$', TireItemListView.as_view(), name='list'),
    url(r'^/?(?P<category_uri>tires)/d(?P<diameter>[\d\.]+)/$', TireItemListView.as_view(), name='list'),
    url(r'^/?(?P<category_uri>tires)/brand/(?P<brand>[\w\d\-\s\%\(\)]+)/$', TireItemListView.as_view(), name='list'),
    url(r'^/?(?P<category_uri>tires)/brand/(?P<brand>[\w\d\-\s\%\(\)]+)/d(?P<diameter>[\d\.]+)/$', TireItemListView.as_view(), name='list'),
    url(r'^/?(?P<category_uri>tires)/brand/(?P<brand>[\w\d\-\s\%\(\)]+)/(?P<pk>\d+)/$', TireDetailView.as_view(), name='detail'),
    url(r'^$/?', KolesoIndexView.as_view(), name='index'),

    # url(r'^/?(?P<category_id>\d+)/$', KolesoItemListView.as_view(), name='list'),
    # url(r'^/?(?P<category_id>\d+)/(?P<pk>\d+)/$', DiskDetailView.as_view(), name='detail'),
)

# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
