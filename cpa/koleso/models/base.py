#  coding: utf8
from __future__ import unicode_literals

from django.db import models


class ItemManager(models.Manager):
    def active_items(self):
        return self.model.objects.filter(available=True, enabled=True)


class BaseCatalogItem(models.Model):
    country_of_origin = models.CharField(max_length=100, blank=True, null=True)
    condition = models.CharField(max_length=64, blank=True, null=True)
    enabled = models.BooleanField(default=False)
    available = models.BooleanField(default=False)
    picture = models.URLField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    oldprice = models.FloatField(max_length=10, blank=True, null=True, default=0)
    price = models.FloatField(max_length=10, blank=True, null=True, default=0)
    modified_time = models.DateTimeField(blank=True, null=True)
    category = models.ForeignKey('CatalogCategory', null=True)
    url = models.URLField(max_length=512, blank=True, null=True)
    param = models.CharField(max_length=512, blank=True, null=True)
    sales_notes = models.CharField(max_length=512, blank=True, null=True)
    item_type = models.CharField(max_length=512, blank=True, null=True)
    item_id = models.CharField(max_length=128, blank=True, default='')
    name = models.CharField(max_length=512, blank=True, null=True)

    objects = ItemManager()

    def __unicode__(self):
        return self.name

    @property
    def discount(self):
        if not self.oldprice or self.oldprice <= self.price:
            return 0
        return self.oldprice - self.price

    @property
    def discount_percent(self):
        if not self.discount:
            return 0
        return "%d" % int(float(self.discount * 100) // float(self.oldprice))


    class Meta:
        abstract = True
