# coding: utf-8
import csv
from datetime import datetime

from django.core.management.base import BaseCommand
from django.conf import settings

import pytz

from cpa.koleso.models import CatalogVendor as Vendor, CatalogCategory as Category, CatalogItem


# """
# Скачивание файла
# Импорт файла
#     - подсчет кол-ва товаров в файле
# """

class Command(BaseCommand):
    # сделать отдельной процедурой подсчет(перерасчет) кол-ва товаров в категории
    def handle(self, *args, **options):
        csv_reader('catalog_new3.csv', import_sportiv)
        # print "amount: %d, created: %d, updated: %d" % (cnt, created_cnt, updated_cnt)


def import_sportiv(row):
    ALLOWED_FIELDS = (
        'available',
        'picture',
        'country_of_origin',
        'vendor',
        'description',
        'oldprice',
        'price',
        'modified_time',
        'category',
        'url',
        'param',
        'sales_notes',
        'itemType',
        'name',
        'categoryId',
        'id',
    )

    row_columns = row.keys()
    for field_name in row_columns:
        if not field_name in ALLOWED_FIELDS:
            del row[field_name]

    row = rename_dict_keys(row,
        (('categoryId', 'category_id'), ('id', 'item_id'),)
    )

    vendor, _ = Vendor.objects.get_or_create(name=row['vendor'].strip().capitalize())
    row['vendor'] = vendor
    row['oldprice'] = float(row['oldprice'] or 0)
    row['price'] = float(row['price'] or 0)
    row['modified_time'] = datetime.utcfromtimestamp(int(row['modified_time'])).replace(tzinfo=pytz.utc)

    row['category_id'] = make_categories_tree(row['category_id'])
    row['item_id'] = row['item_id']
    row['enabled'] = True

    row = {col: unicode(val, 'utf-8') if isinstance(val, str) else val for col, val in row.items()}
    item, created = CatalogItem.objects.update_or_create(item_id=row['item_id'], defaults=row)
    counters = {}
    counters['handled'] = 1
    if created:
        counters['created'] = 1
        counters['updated'] = 0
    else:
        counters['created'] = 0
        counters['updated'] = 1
    return counters


def rename_dict_keys(row, new_keys_mapping):
    for key, new_key in new_keys_mapping:
        value = row[key]
        row[new_key] = value
        del row[key]
    return row


def make_categories_tree(categories_string=''):
    """
    делает из строки "Category/subcategory/sub subcategory" дерево категорий в бд
    """
    parent = Category.objects.get(level=0)
    categories = enumerate(categories_string.split('/'), start=0)
    for index, category_name in categories:
        category, created = Category.objects.get_or_create(name=category_name, parent=parent)
        parent = category
    return category.id


def csv_reader(file_name, row_handler):
    file_path = settings.BASE_DIR + '/' + file_name

    with open(file_path, 'r') as line:
        reader = csv.DictReader(line, delimiter=';')

        # # также можно припихнуть проверку на слишком маленкое кол-во товаров
        # cursor.execute('truncate catalog_item')
        handled = 0
        created = 0
        updated = 0
        for row in reader:
            counters = row_handler(row)
            handled += counters['handled']
            created += counters['created']
            updated += counters['updated']
        print "handled: %d, created: %d, updated: %d" % (handled, created, updated)
