# coding: utf8
from django.conf import settings
from django.conf.urls import include, patterns, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps import Sitemap
from django.contrib.sitemaps import views as sitemaps_views
from django.views.defaults import page_not_found
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from cpa.koleso.models import Tire, Disk, TireVendor, DiskVendor


handler404 = lambda request: page_not_found(request, '404.jinja')

class TireSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5
    limit = 1000

    def items(self):
        return Tire.objects.active_items()

    def lastmod(self, obj):
        return obj.modified_time


class DiskSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5
    limit = 1000

    def items(self):
        return Disk.objects.active_items()

    def lastmod(self, obj):
        return obj.modified_time


class TireVendorSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    limit = 1000

    def items(self):
        return TireVendor.objects.all()


class DiskVendorSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5
    limit = 1000

    def items(self):
        return DiskVendor.objects.all()


sitemaps_dict = {
    'sitemaps': {
            'disks': DiskSitemap(),
            'tires': TireSitemap(),
            'tirevendor': TireVendorSitemap(),
            'diskvendor': DiskVendorSitemap(),
        }
}


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
]

sitemaps = {
        'disks': DiskSitemap(),
        'tires': TireSitemap(),
        'tirevendor': TireVendorSitemap(),
        'diskvendor': DiskVendorSitemap(),
    }

urlpatterns += [
    url(r'^sitemap\.xml$',
        cache_page(86400)(sitemaps_views.index),
        {'sitemaps': sitemaps, 'sitemap_url_name': 'sitemaps'}),
    url(r'^sitemap-(?P<section>.+)\.xml$',
        cache_page(86400)(sitemaps_views.sitemap),
        {'sitemaps': sitemaps}, name='sitemaps'),
]

urlpatterns += [
    url(r'^robots.txt$', TemplateView.as_view(template_name='robots.txt.jinja')),
    url(r'', include('cpa.koleso.urls', namespace='koleso')),
    url(r'goods/', include('cpa.avtogsm.urls', namespace='avtogsm')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )